require( "ts-node" ).register( {
    compilerOptions: {
        target: "es5",
        module: "commonjs",
        esModuleInterop: true,
    }
} );

module.exports = require( "./webpack.config.ts" );
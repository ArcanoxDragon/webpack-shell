import path from "path";
import webpack from "webpack";
import findScriptEntryPoints from "./build/lib/findScriptEntryPoints";
import findStyleEntryPoints from "./build/lib/findStyleEntryPoints";

import { loader as MiniCssExtractLoader } from "mini-css-extract-plugin";
import { CleanWebpackPlugin } from "clean-webpack-plugin";

import TsconfigPathsPlugin from "tsconfig-paths-webpack-plugin";
import CopyLibsPlugin from "./build/plugins/CopyLibsPlugin";
import HardSourcePlugin from "hard-source-webpack-plugin";
import MiniCssExtractPlugin from "mini-css-extract-plugin";
import TerserPlugin from "terser-webpack-plugin";
import ForkTsCheckerWebpackPlugin from "fork-ts-checker-webpack-plugin";

interface Environment {
    prod: boolean;
    cleanLibs: boolean;
}

const DefaultEnvironment: Environment = {
    prod: false,
    cleanLibs: false,
};

export default function buildConfiguration( env: Environment ): webpack.Configuration {
    const { prod, cleanLibs } = { ...DefaultEnvironment, ...env };
    const mode = prod ? "production" : "development";
    const sourceMap = !prod;
    const outputPath = path.join( __dirname, "../wwwroot/bundle" );

    return {
        mode,
        devtool: sourceMap ? "inline-source-map" : false,

        entry: {
            "js/manifest": [ "babel-polyfill" ],
            ...findScriptEntryPoints(),
            ...findStyleEntryPoints(),
        },

        module: {
            rules: [
                {
                    // Lint TypeScript code

                    test: /\.tsx?$/i,
                    enforce: "pre",
                    exclude: /\.d\.ts$/i,
                    use: [
                        {
                            loader: "eslint-loader",
                            options: {
                                cache: true,
                                emitWarning: true,
                            },
                        },
                    ],
                },
                {
                    // Compile TypeScript code

                    test: /\.tsx?$/i,
                    use: [
                        { loader: "thread-loader" },
                        { loader: "babel-loader" },
                        {
                            loader: "ts-loader",
                            options: {
                                happyPackMode: true,
                                transpileOnly: true,
                                reportFiles: [
                                    "src/**/*.{ts,tsx}",
                                ],
                                compilerOptions: {
                                    sourceMap,
                                },
                            },
                        },
                    ],
                },
                {
                    // Compile SCSS styles

                    test: /\.scss$/i,
                    use: [
                        MiniCssExtractLoader,
                        "css-loader",
                        {
                            loader: "sass-loader",
                            options: { sourceMap },
                        },
                    ],
                },
            ],
        },

        optimization: {
            namedModules: true,
            minimize: prod,
            minimizer: [ new TerserPlugin( {
                exclude: /node_modules\//i,
            } ) ],
            runtimeChunk: {
                name: "js/manifest",
            },
            splitChunks: {
                name( module: webpack.compilation.Module, chunks: webpack.compilation.Chunk[] ) {
                    function getCleanChunkName( chunk: webpack.compilation.Chunk ) {
                        const nameMatch = chunk.name.match( /.*?([^/]+)$/i );

                        if ( !nameMatch )
                            throw new Error( `Invalid chunk name: ${chunk.name}` );

                        return nameMatch[ 1 ];
                    }

                    return `js/chunks/${chunks.map( getCleanChunkName ).join( "~" )}`;
                },

                cacheGroups: {
                    vendor: {
                        name: "js/vendor",
                        chunks: "initial",
                        test: /[\\/]node_modules[\\/]/i,
                        priority: -10,
                    },
                    common: {
                        name: "js/common",
                        chunks: "all",
                        minChunks: 2,
                        priority: -20,
                        reuseExistingChunk: true,
                    },
                },
            },
        },

        output: {
            path: outputPath,
            publicPath: "/bundle/",
            filename: "[name].js",
            chunkFilename: "[name].js?v=[chunkhash]",
        },

        plugins: [
            new CleanWebpackPlugin( {
                cleanOnceBeforeBuildPatterns: cleanLibs ? [ "js", "css", "lib" ] : [ "js", "css" ],
                cleanAfterEveryBuildPatterns: [ "css/**/*.{js,js.map}" ],
                cleanStaleWebpackAssets: false,
            } ),

            new CopyLibsPlugin( {
                libs: [
                    // { name: "lib-module-name", globs: [ "dist/js/*.min.js" ] },
                ],
            } ),

            new MiniCssExtractPlugin( {
                filename: "[name].css",
                chunkFilename: "[name].css?v=[chunkhash]",
            } ),

            new HardSourcePlugin(),

            new ForkTsCheckerWebpackPlugin( {
                checkSyntacticErrors: true,
                reportFiles: [
                    "src/**/*.{ts,tsx}",
                ],
            } ),

            new HardSourcePlugin.ExcludeModulePlugin( [
                { test: /sass-loader|css-loader/i },
            ] ),
        ],

        resolve: {
            extensions: [ ".js", ".ts", ".tsx" ],
            plugins: [ new TsconfigPathsPlugin() ],
        },
    };
}
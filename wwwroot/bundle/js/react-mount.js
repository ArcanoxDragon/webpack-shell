(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["js/react-mount"],{

/***/ "./src/main/react-mount.ts":
/*!*********************************!*\
  !*** ./src/main/react-mount.ts ***!
  \*********************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _arcanox_asp_net_core_helpers__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @arcanox/asp-net-core-helpers */ "./node_modules/@arcanox/asp-net-core-helpers/Dist/index.js");
function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }



function resolveApp(_x) {
  return _resolveApp.apply(this, arguments);
}

function _resolveApp() {
  _resolveApp = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee(appName) {
    var appModule;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return __webpack_require__("./src/react/apps lazy recursive ^\\.\\/.*$")("./".concat(appName));

          case 2:
            appModule = _context.sent;
            return _context.abrupt("return", appModule["default"]);

          case 4:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _resolveApp.apply(this, arguments);
}

$(function () {
  _arcanox_asp_net_core_helpers__WEBPACK_IMPORTED_MODULE_0__["ReactHelpers"].findAndMountApps(resolveApp);
});

/***/ }),

/***/ "./src/react/apps lazy recursive ^\\.\\/.*$":
/*!*******************************************************!*\
  !*** ./src/react/apps lazy ^\.\/.*$ namespace object ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./folder.txt": [
		"./src/react/apps/folder.txt",
		"js/react/folder-txt"
	]
};
function webpackAsyncContext(req) {
	if(!__webpack_require__.o(map, req)) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}

	var ids = map[req], id = ids[0];
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__.t(id, 7);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./src/react/apps lazy recursive ^\\.\\/.*$";
module.exports = webpackAsyncContext;

/***/ })

},[["./src/main/react-mount.ts","js/manifest","js/vendor"]]]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9zcmMvbWFpbi9yZWFjdC1tb3VudC50cyIsIndlYnBhY2s6Ly8vLi9zcmMvcmVhY3QvYXBwcyBsYXp5IF5cXC5cXC8uKiQgbmFtZXNwYWNlIG9iamVjdCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQ0E7O1NBRWUsVTs7Ozs7OzswQkFBZixpQkFBMkIsT0FBM0I7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFDMEIsOEVBQXNFLE9BQXRFLEVBRDFCOztBQUFBO0FBQ1EscUJBRFI7QUFBQSw2Q0FHVyxTQUFTLFdBSHBCOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEc7Ozs7QUFNQSxDQUFDLENBQUUsWUFBSztBQUNKLDRFQUFZLENBQUMsZ0JBQWIsQ0FBK0IsVUFBL0I7QUFDSCxDQUZBLENBQUQsQzs7Ozs7Ozs7Ozs7QUNUQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQyIsImZpbGUiOiJqcy9yZWFjdC1tb3VudC5qcz92PTM4Njg1MWU5ODU1NzFhZDVlOTFiIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50VHlwZSB9IGZyb20gXCJyZWFjdFwiO1xyXG5pbXBvcnQgeyBSZWFjdEhlbHBlcnMgfSBmcm9tIFwiQGFyY2Fub3gvYXNwLW5ldC1jb3JlLWhlbHBlcnNcIjtcclxuXHJcbmFzeW5jIGZ1bmN0aW9uIHJlc29sdmVBcHAoIGFwcE5hbWU6IHN0cmluZyApOiBQcm9taXNlPENvbXBvbmVudFR5cGU+IHtcclxuICAgIGxldCBhcHBNb2R1bGUgPSBhd2FpdCBpbXBvcnQoIC8qIHdlYnBhY2tDaHVua05hbWU6IFwianMvcmVhY3QvW3JlcXVlc3RdXCIgKi8gYC4uL3JlYWN0L2FwcHMvJHthcHBOYW1lfWAgKTtcclxuXHJcbiAgICByZXR1cm4gYXBwTW9kdWxlLmRlZmF1bHQ7XHJcbn1cclxuXHJcbiQoICgpID0+IHtcclxuICAgIFJlYWN0SGVscGVycy5maW5kQW5kTW91bnRBcHBzKCByZXNvbHZlQXBwICk7XHJcbn0gKTsiLCJ2YXIgbWFwID0ge1xuXHRcIi4vZm9sZGVyLnR4dFwiOiBbXG5cdFx0XCIuL3NyYy9yZWFjdC9hcHBzL2ZvbGRlci50eHRcIixcblx0XHRcImpzL3JlYWN0L2ZvbGRlci10eHRcIlxuXHRdXG59O1xuZnVuY3Rpb24gd2VicGFja0FzeW5jQ29udGV4dChyZXEpIHtcblx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhtYXAsIHJlcSkpIHtcblx0XHRyZXR1cm4gUHJvbWlzZS5yZXNvbHZlKCkudGhlbihmdW5jdGlvbigpIHtcblx0XHRcdHZhciBlID0gbmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIiArIHJlcSArIFwiJ1wiKTtcblx0XHRcdGUuY29kZSA9ICdNT0RVTEVfTk9UX0ZPVU5EJztcblx0XHRcdHRocm93IGU7XG5cdFx0fSk7XG5cdH1cblxuXHR2YXIgaWRzID0gbWFwW3JlcV0sIGlkID0gaWRzWzBdO1xuXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXy5lKGlkc1sxXSkudGhlbihmdW5jdGlvbigpIHtcblx0XHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXy50KGlkLCA3KTtcblx0fSk7XG59XG53ZWJwYWNrQXN5bmNDb250ZXh0LmtleXMgPSBmdW5jdGlvbiB3ZWJwYWNrQXN5bmNDb250ZXh0S2V5cygpIHtcblx0cmV0dXJuIE9iamVjdC5rZXlzKG1hcCk7XG59O1xud2VicGFja0FzeW5jQ29udGV4dC5pZCA9IFwiLi9zcmMvcmVhY3QvYXBwcyBsYXp5IHJlY3Vyc2l2ZSBeXFxcXC5cXFxcLy4qJFwiO1xubW9kdWxlLmV4cG9ydHMgPSB3ZWJwYWNrQXN5bmNDb250ZXh0OyJdLCJzb3VyY2VSb290IjoiIn0=
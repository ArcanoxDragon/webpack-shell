### What is this repository for? ###

This repository contains a simple Webpack shell project that will, with little to no configuration, compile the following:

- TypeScript (+ React) files in src/
- SCSS files in styles/


Script entry points will be discovered with the glob `src/main/**/*.{ts,tsx}`. For example, the file `src/main/pages/home.ts` will be compiled to `{output}/bundle/js/pages/home.js`.

React apps will be discovered with the glob `src/react/**/*.tsx` and can be mounted with Arcanox's ASP.NET Core Helpers library.

Style entry points will be discovered with the glob `styles/**/*.scss` (files starting with an underscore, such as `common/_partial.scss`, will not be treated as an entry point). For example, the style `styles/pages/home.scss` will be compiled to `{output}/css/pages/home.css`.